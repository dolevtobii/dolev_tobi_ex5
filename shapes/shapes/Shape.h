#pragma once
#include "Point.h"
#include "Canvas.h"
#include <string>

class Shape 
{
public:
	Shape(const std::string& name, const std::string& type);
	~Shape();
	virtual double getArea() const;
	virtual double getPerimeter() const;
	virtual void draw(const Canvas& canvas);
	virtual void move(const Point& other); 
	virtual void printDetails() const;
	virtual std::string getType() const;
	virtual std::string getName() const;

	virtual void clearDraw(const Canvas& canvas);

private:
	std::string _name;
	std::string _type;
};