#pragma once
#include "Polygon.h"

class Arrow : public Shape
{
public:
	Arrow(const Point& a, const Point& b, const std::string& type, const std::string& name);
	~Arrow();

	void draw(const Canvas& canvas);
	void clearDraw(const Canvas& canvas);

	virtual double getArea() const;
	virtual double getPerimeter() const;
	virtual void move(const Point& other);

	virtual void printDetails() const;

	
private:
	std::vector<Point> _points;
};