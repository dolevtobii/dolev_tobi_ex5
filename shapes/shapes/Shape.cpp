#include "Shape.h"
#include <iostream>


/*
	function will build the class for work
	input: type- the type of the shape
		   name- the name of the shape
	output: none
*/
Shape::Shape(const std::string& name, const std::string& type)
{
	this->_name = name;
	this->_type = type;
}


/*
	function will delete the class
	input: none
	ountput: none
*/
Shape::~Shape()
{

}


/*
	function will get the area of the shape
	input: none
	ountput: the area of the shape
*/
double Shape::getArea() const
{
	return 0.0;
}


/*
	function will get the perimeter of the shape
	input: none
	ountput: the area of the shape
*/
double Shape::getPerimeter() const
{
	return 0.0;
}


/*
	function will draw the shape on canvas
	input: the canvas
	ountput: none
*/
void Shape::draw(const Canvas& canvas)
{
}


/*
	function will move the shape to the location
	input: none
	ountput: none
*/
void Shape::move(const Point& other)
{
}


/*
	function will print the details of the shape
	input: none
	ountput: none
*/
void Shape::printDetails() const
{
}


/*
	function will return the shape type 
	input: none
	output: the shape type
*/
std::string Shape::getType() const
{
	return this->_type;
}


/*
	function will return the shape name
	input: none
	output: the shape name
*/
std::string Shape::getName() const
{
	return this->_name;
}


/*
	function will delete the shape from canvas
	input: the canvas
	ountput: none
*/
void Shape::clearDraw(const Canvas& canvas)
{
	
}
