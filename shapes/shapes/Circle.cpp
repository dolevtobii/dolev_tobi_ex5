#include "Circle.h"
#include <iostream>

using std::cout;



/*
	function will build the class for work
	input: center- the point of the center
		   radius- the radius of the circle
		   type- the type of the shape
		   name- the name of the shape
	output: none
*/
Circle::Circle(const Point& center, double radius, const std::string& type, const std::string& name) : Shape(name, type)
{
	this->_center.push_back(center);
	this->_radius = radius;
}


/*
	function will delete the class
	input: none
	ountput: none
*/
Circle::~Circle()
{
}

/*
	function will retuen the center of the circle
	input: none
	ountput: the center
*/
const Point& Circle::getCenter() const
{
	return this->_center[0];
}


/*
	function will retuen the radius of the circle
	input: none
	ountput: the radius
*/
double Circle::getRadius() const
{
	return this->_radius;
}


/*
	function will draw the shape on canvas
	input: the canvas
	ountput: none
*/
void Circle::draw(const Canvas& canvas)
{
	canvas.draw_circle(getCenter(), getRadius());
}


/*
	function will delete the shape from canvas
	input: the canvas
	ountput: none
*/
void Circle::clearDraw(const Canvas& canvas)
{
	canvas.clear_circle(getCenter(), getRadius());
}


/*
	function will get the area of the circle
	input: none
	ountput: the area of the shape
*/
double Circle::getArea() const
{
	return PI * this->_radius * this->_radius;
}


/*
	function will get the perimeter of the circle
	input: none
	ountput: the area of the shape
*/
double Circle::getPerimeter() const
{
	return 2 * PI * this->_radius;
}


/*
	function will move the circle to the location
	input: none
	ountput: none
*/
void Circle::move(const Point& other)
{
	this->_center[0] = other;
}


/*
	function will print the details of the circle
	input: none
	ountput: none
*/
void Circle::printDetails() const
{
	cout << this->getType() << "  " << this->getName() << "          " << this->getArea() << "  " << this->getPerimeter();
	system("pause");
}


