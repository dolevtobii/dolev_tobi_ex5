#include "Polygon.h"
#include <iostream>
#include <string>
#include "Shape.h"


/*
	function will build the class for work
	input: a- point 1
		   b- point 2
		   type- the type of the shape
		   name- the name of the shape
	output: none
*/
Polygon::Polygon(const std::string& type, const std::string& name) : Shape(name, type)
{
	
}


/*
	function will delete the class
	input: none
	ountput: none
*/
Polygon::~Polygon()
{
}
