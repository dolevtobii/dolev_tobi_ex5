#include <iostream>
#include <string>
#include "Arrow.h"
#include "Shape.h"


using std::cout;
using std::cin;
using std::endl;
using std::string;


/*
	function will build the class for work
	input: a- point 1
		   b- point 2
		   type- the type of the shape
		   name- the name of the shape
	output: none
*/
Arrow::Arrow(const Point& a, const Point& b, const std::string& type, const std::string& name) : Shape(name, type)
{
	this->_points.push_back(a);
	this->_points.push_back(b);
}


/*
	function will delete the class
	input: none
	ountput: none
*/
Arrow::~Arrow()
{

}


/*
	function will draw the shape on canvas
	input: the canvas
	ountput: none
*/
void Arrow::draw(const Canvas& canvas)
{
	canvas.draw_arrow(_points[0], _points[1]);
}


/*
	function will delete the shape from canvas
	input: the canvas
	ountput: none
*/
void Arrow::clearDraw(const Canvas& canvas)
{
	canvas.clear_arrow(_points[0], _points[1]);
}


/*
	function will get the area of the arrow
	input: none
	ountput: the area of the shape
*/
double Arrow::getArea() const
{
	return 0.0;
}


/*
	function will get the perimeter of the arrow
	input: none
	ountput: the area of the shape
*/
double Arrow::getPerimeter() const
{
	return this->_points[0].distance(this->_points[1]);
}


/*
	function will move the arrow to the location
	input: none
	ountput: none
*/
void Arrow::move(const Point& other)
{
	this->_points[0] = other;
}


/*
	function will print the details of the arrow
	input: none
	ountput: none
*/
void Arrow::printDetails() const
{
	cout << this->getType() << "  " << this->getName() << "          " << this->getArea() << "  " << this->getPerimeter();
	system("pause");
}


