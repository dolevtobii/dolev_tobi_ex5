#include "Triangle.h"
#include <iostream>

using std::cerr;
using std::endl;


/*
	function will build the class for work
	input: a- point 1
		   b- point 2
		   c- point 3
		   type- the type of the shape
		   name- the name of the shape
	output: none
*/
Triangle::Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name) : Polygon(type, name)
{
	if ((a.getX() == b.getX()  &&  b.getX() == c.getX()) || (a.getY() == b.getY() && b.getY() == c.getY()) || (a.getX() == b.getX() && a.getY() == b.getY()) || (a.getX() == c.getX() && a.getY() == c.getY()) || (b.getX() == c.getX() && b.getY() == c.getY()))
	{
		cerr << "\ncant build a Triangle with 3 points on the same line\n" << endl;
		exit(0);
	}
	else 
	{

		this->_points.push_back(a);
		this->_points.push_back(b);
		this->_points.push_back(c);
		
	}
}


/*
	function will delete the class
	input: none
	ountput: none
*/
Triangle::~Triangle()
{
}


/*
	function will draw the shape on canvas
	input: the canvas
	ountput: none
*/
void Triangle::draw(const Canvas& canvas)
{
	canvas.draw_triangle(_points[0], _points[1], _points[2]);
}


/*
	function will delete the shape from canvas
	input: the canvas
	ountput: none
*/
void Triangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_triangle(_points[0], _points[1], _points[2]);
}


/*
	function will get the area of the Triangle
	input: none
	ountput: the area of the shape
*/
double Triangle::getArea() const
{
	double a = this->_points[0].distance(this->_points[1]);
	double b = this->_points[1].distance(this->_points[2]);
	double c = this->_points[2].distance(this->_points[0]);
	
	double p = this->getPerimeter();
	double result = sqrt(p * (p - a) * (p - b) * (p - c));

	return result;
}


/*
	function will get the perimeter of the Triangle
	input: none
	ountput: the area of the shape
*/
double Triangle::getPerimeter() const
{

	double a = this->_points[0].distance(this->_points[1]);
	double b = this->_points[1].distance(this->_points[2]);
	double c = this->_points[2].distance(this->_points[0]);

	return a + b + c;
}


/*
	function will move the Triangle to the location
	input: none
	ountput: none
*/
void Triangle::move(const Point& other)
{
	this->_points[0] = other;
	Point a(other.getX() - this->_points[0].distance(this->_points[1]), other.getY() - this->_points[0].distance(this->_points[1]));
	Point b(other.getX() - this->_points[1].distance(this->_points[2]), other.getY() - this->_points[1].distance(this->_points[2]));
	this->_points[1] = a;
	this->_points[2] = b;
}


/*
	function will print the details of the triangle
	input: none
	ountput: none
*/
void Triangle::printDetails() const
{
	std::cout << this->getType() << "  " << this->getName() << "          " << this->getArea() << "  " << this->getPerimeter();
	system("pause");
}

