#include "Point.h"
#include <math.h>


/*
	function will build the class for work
	input: x- x param
		   y- y param
	output: none
*/
Point::Point(double x, double y)
{
	this->_x = x;
	this->_y = y;
}


/*
	function will copy point to class
	input: other- point
	output: none
*/
Point::Point(const Point& other)
{
	this->_x = other._x;
	this->_y = other._y;
}



/*
	function will delete the class
	input: none
	ountput: none
*/
Point::~Point()
{

}


/*
	function will add the point to the given point
	input: other- point
	output: the new point
*/
Point Point::operator+(const Point& other) const
{
	Point result = *this; 
	result += other;
	return result;
}


/*
	function will add the values of the point to the given point
	input: other- point
	output: the new point
*/
Point& Point::operator+=(const Point& other)
{
	this->_x += other._x;
	this->_y += other._y;
	
	return *this;
}


/*
	function will return x value
	input: none
	output: x value
*/
double Point::getX() const
{
	return this->_x;
}


/*
	function will return y value
	input: none
	output: y value
*/
double Point::getY() const
{
	return this->_y;
}


/*
	function will return the distance between 2 points
	input: other- point
	output: the distance
*/
double Point::distance(const Point& other) const
{
	double result = 0;
	double xParam = this->_x - other._x;
	double yParam = this->_y - other._y;

	result = sqrt(pow(xParam, 2) + pow(yParam, 2));

	return result;
}
