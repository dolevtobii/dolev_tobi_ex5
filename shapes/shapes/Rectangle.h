#pragma once
#include "Polygon.h"


namespace myShapes
{
	// Calling it MyRectangle becuase Rectangle is taken by global namespace.
	class Rectangle : public Polygon
	{
	public:
		Rectangle(const Point& a, double length, double width, const std::string& type, const std::string& name);
		virtual ~Rectangle();

		virtual void draw(const Canvas& canvas);
		virtual void clearDraw(const Canvas& canvas);
		
		virtual double getArea() const;
		virtual double getPerimeter() const;
		virtual void move(const Point& other);
		
		virtual void printDetails() const;

	private:
		double _width;
		double _length;
	};
}