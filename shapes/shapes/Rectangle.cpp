#include "Rectangle.h"
#include "Polygon.h"
#include <iostream>
#include <math.h>

using std::cerr;
using std::endl;



/*
	function will build the class for work
	input: a- point 1
		   length- the length of the rectangle
		   width- the width of the rectangle
		   type- the type of the shape
		   name- the name of the shape
	output: none
*/
myShapes::Rectangle::Rectangle(const Point& a, double length, double width, const std::string& type, const std::string& name) : Polygon(type, name)
{
	if(length > 0 && width > 0)
	{
		Point b(a.getX() + width, a.getY() + length);
		this->_points.push_back(a);
		this->_points.push_back(b);
		this->_width = width;
		this->_length = length;
	}
	else
	{
		cerr << "\ncant build a Rectangle with negative side values!\n" << endl;
		exit(0);
	}

}


/*
	function will delete the class
	input: none
	ountput: none
*/
myShapes::Rectangle::~Rectangle()
{
	
}


/*
	function will draw the shape on canvas
	input: the canvas
	ountput: none
*/
void myShapes::Rectangle::draw(const Canvas& canvas)
{
	canvas.draw_rectangle(_points[0], _points[1]);
}


/*
	function will delete the shape from canvas
	input: the canvas
	ountput: none
*/
void myShapes::Rectangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_rectangle(_points[0], _points[1]);
}


/*
	function will get the area of the rectangle
	input: none
	ountput: the area of the shape
*/
double myShapes::Rectangle::getArea() const
{
	return this->_width * this->_length;
}


/*
	function will get the perimeter of the rectangle
	input: none
	ountput: the area of the shape
*/
double myShapes::Rectangle::getPerimeter() const
{
	return pow(this->_width, 2) + pow(this->_length, 2);
}


/*
	function will move the rectangle to the location
	input: none
	ountput: none
*/
void myShapes::Rectangle::move(const Point& other)
{
	this->_points[0] = other;
	Point b(other.getX() + this->_width, other.getY() + this->_length);
	this->_points[1] = b;
}


/*
	function will print the details of the rectangle
	input: none
	ountput: none
*/
void myShapes::Rectangle::printDetails() const
{
	std::cout << this->getType() << "  " << this->getName() << "          " << this->getArea() << "  " << this->getPerimeter();
	system("pause");
}

