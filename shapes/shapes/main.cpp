#include <iostream>
#include <string>
#include <vector>
#include "Menu.h"
#include "Arrow.h"
#include "Point.h"
#include "Shape.h"
#include "Circle.h"
#include "Polygon.h"
#include "Rectangle.h"
#include "Triangle.h"

using std::cout;
using std::cin;
using std::endl;
using std::string;

#define MAX_ARRAY 100

int main()
{
	Canvas c;
	Menu m;

	Shape* arr[MAX_ARRAY];
	
	int i = 0;

	int userChoice = 0;
	int userChoice2 = 0;
	int userChoice3 = 0;
	int userChoice4 = 0;
	int count = 0;
	double	X = 0;
	double Y = 0;
	double	R = 0;

	string nameOfShape = "";

	bool flag = false;

	do
	{
		
		m.menu();
		cin >> userChoice;


		if (userChoice == 0)
		{
			m.menu2();
			cin >> userChoice2;

			if (userChoice2 == 0)
			{

				cout << "Please enter the X of the center of the Circle:" << endl;
				cin >> X;
				cout << "Please enter the Y of the center of the Circle:" << endl;
				cin >> Y;
				Point p1(X, Y);
				cout << "Please enter the radius of the center of the Circle:" << endl;
				cin >> R;
				
				cout << "Enter the name of the shape:" << endl;
				cin >> nameOfShape;
				
				arr[count] = new Circle(p1, R, "Circle", nameOfShape);
				arr[count]->draw(c);
				count++;
			}
			else if (userChoice2 == 1)
			{

				cout << "Please enter the X1 of the Arrow:" << endl;
				cin >> X;
				cout << "Please enter the Y1 of the Arrow:" << endl;
				cin >> Y;
				Point p1(X, Y);

				cout << "Please enter the X2 of the Arrow:" << endl;
				cin >> X;
				cout << "Please enter the Y2 of the Arrow:" << endl;
				cin >> Y;
				Point p2(X, Y);

				cout << "Enter the name of the shape:" << endl;
				cin >> nameOfShape;

				arr[count] = new Arrow(p1, p2, "Arrow", nameOfShape);
				arr[count]->draw(c);
				count++;
			}
			else if (userChoice2 == 2)
			{
				cout << "Please enter the X1 of the Triangle:" << endl;
				cin >> X;
				cout << "Please enter the Y1 of the Triangle:" << endl;
				cin >> Y;
				Point p1(X, Y);

				cout << "Please enter the X2 of the Triangle:" << endl;
				cin >> X;
				cout << "Please enter the Y2 of the Triangle:" << endl;
				cin >> Y;
				Point p2(X, Y);

				cout << "Please enter the X3 of the Triangle:" << endl;
				cin >> X;
				cout << "Please enter the Y3 of the Triangle:" << endl;
				cin >> Y;
				Point p3(X, Y);

				cout << "Enter the name of the shape:" << endl;
				cin >> nameOfShape;

				arr[count] = new Triangle(p1, p2, p3, "Triangle", nameOfShape);
				arr[count]->draw(c);
				count++;
			}
			else if (userChoice2 == 3)
			{
				cout << "Please enter the X1 of the Rectangle:" << endl;
				cin >> X;
				cout << "Please enter the Y1 of the Rectangle:" << endl;
				cin >> Y;
				Point p1(X, Y);

				cout << "Please enter the length of the Rectangle:" << endl;
				cin >> X;
				cout << "Please enter the width of the Rectangle:" << endl;
				cin >> Y;

				cout << "Enter the name of the shape:" << endl;
				cin >> nameOfShape;

				arr[count] = new myShapes::Rectangle(p1, X, Y, "Rectangle", nameOfShape);
				arr[count]->draw(c);
				count++;
			}
			flag = true;
		}
		else if(userChoice == 1 && flag)
		{
			for (i = 0; i < count; i++)
			{
				cout << "Enter " << i << " for " << arr[i]->getName() << "(" << arr[i]->getType() << ")" << endl;
			}

			cin >> userChoice3;

			m.menu3();
			cin >> userChoice4;
			if (userChoice4 == 0)
			{
				cout << "Please enter the X moving scale:";
				cin >> X;

				cout << "Please enter the Y moving scale:";
				cin >> Y;

				Point p1(X, Y);
				for (i = 0; i < count; i++)
				{
					arr[i]->clearDraw(c);
					arr[userChoice3]->move(p1);
					arr[i]->draw(c);
				}
			}
			else if(userChoice4 == 1)
			{
				arr[userChoice3]->printDetails();
			}
			else if (userChoice4 == 2)
			{
				arr[userChoice3]->clearDraw(c);
				for (i = 0; i + 1 < count; i++)
				{
					arr[i] = arr[i + 1];
				}
				count--;
			}
			
		}
		else if (userChoice == 2)
		{
			for (i = 0; i < count; i++)
			{
				arr[i]->clearDraw(c);
				delete arr[i];
			}
			count = 0;
		}
		else if (userChoice != 3)
		{
			cout << "Wrong number! try again!(0-3)" << endl;
		}
	} while (userChoice != 3);

	if (_CrtDumpMemoryLeaks())
	{
		std::cout << "Memory leaks!\n";
	}
	else
	{
		std::cout << "No leaks\n";
	}
	system("pause");
	
	return 0;
}